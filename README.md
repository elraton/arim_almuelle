# ARIM ALMUELLE

Photograph landing page.

## Installation

Use the node package manager.

```bash
npm install
```

## For development

```bash
npm run watch
```
## For Production

```bash
npm run build
```
Upload the dist folder to your server.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)